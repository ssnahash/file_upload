from django import forms


class DFileForm(forms.Form):
    file = forms.FileField(
        label='ВЫберите файл',
        error_messages={'required': 'Укажите файл!'},
    )

