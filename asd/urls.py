from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^details/(?P<file_id>\d+)/$', views.details, name='details'),
    url(r'^uploads/$', views.uploads, name='uploads'),
    url(r'^delete/(?P<file_id>\d+)/$', views.delete, name='delete'),
    url(r'^download/(?P<link>\w+)/$', views.download, name='download'),
]