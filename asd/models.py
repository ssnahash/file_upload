import os
import uuid
import shutil

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


def user_directory_path(instance, filename):
    ext = os.path.splitext(filename)[1]
    fname = str(uuid.uuid4()) + ext
    prefix = 'user_{0}'.format(instance.owner.id)
    return os.path.join(prefix, fname)


class DFile(models.Model):
    owner = models.ForeignKey(User)
    path = models.FileField(upload_to=user_directory_path)
    filename = models.CharField(max_length=256)
    sha_sum = models.CharField(max_length=50, editable=False, default=None)
    link = models.BooleanField(default=False)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['uploaded_at', 'filename']

    def delete(self, *args, **kwargs):
        try:
            this_record = DFile.objects.get(id=self.id)
        except (DFile.DoesNotExist, ValueError):
            pass
        else:
            if not this_record.link:
                try:
                    link = DFile.objects.filter(sha_sum=this_record.sha_sum)[0]
                except IndexError:
                    this_record.path.delete()
                else:
                    os.makedirs(os.path.join(settings.MEDIA_ROOT, 'user_{0}'.format(link.owner.id)), exist_ok=True)

                    new_path_list = list(os.path.split(str(this_record.path)))
                    new_path_list[0] = 'user_{0}'.format(link.owner.id)
                    new_path = os.path.join(new_path_list[0], new_path_list[1])

                    shutil.move(
                        os.path.join(settings.MEDIA_ROOT, str(this_record.path)),
                        os.path.join(settings.MEDIA_ROOT, new_path)
                    )

                    link.path = new_path
                    link.link = 0
                    link.save()

                    DFile.objects.filter(
                        sha_sum=this_record.sha_sum,
                        link=True
                    ).update(path=new_path)
        finally:
            super(DFile, self).delete(*args, **kwargs)
