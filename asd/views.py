from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.conf import settings

import os
import hashlib

from .models import DFile
from .forms import DFileForm


@login_required
def index(request):
    files = DFile.objects.filter(owner=request.user).all()
    context = {'files': files}
    return render(request, 'asd/index.html', context)


@login_required
def uploads(request):
    count = DFile.objects.all().count()
    if count > 100:
        msg = "Превышение количества файлов (100)!"
        messages.warning(request, msg)
        return HttpResponseRedirect(reverse('asd:index'))
    if request.method == 'POST':
        form = DFileForm(request.POST, request.FILES)
        if form.is_valid():
            newfile = DFile(path=request.FILES['file'])
            newfile.owner = request.user
            newfile.filename = request.FILES['file'].name

            sha = hashlib.sha1()
            for chunk in newfile.path.chunks():
                sha.update(chunk)
            newfile.sha_sum = sha.hexdigest()
            files = DFile.objects.filter(
                    sha_sum=newfile.sha_sum,
                    link=False
                )
            msg = "Файл {0} загружен!".format(newfile.filename)
            for file in files:
                newfile.path = file.path
                newfile.link = True
                msg = "Файл {0} найден у другого пользователя!".format(newfile.filename)
                if file.owner == newfile.owner:
                    msg = "Файл {0} у вас уже есть и имеет имя {1}!".format(newfile.filename, file.filename)
                    messages.warning(request, msg)
                    return HttpResponseRedirect(reverse('asd:index'))
                # Проверка на существование ссылок на загружаемый файл
                # если ссылка на этот файл уже существует у данного пользователя
                link_files = DFile.objects.filter(
                    sha_sum=newfile.sha_sum,
                    owner=newfile.owner
                )
                for link_file in link_files:
                    msg = "Файл {0} у вас уже есть и имеет имя {1}!".format(newfile.filename, file.filename)
                    messages.warning(request, msg)
                    return HttpResponseRedirect(reverse('asd:index'))
            newfile.save()
            messages.success(request, msg)
            return HttpResponseRedirect(reverse('asd:index'))
    else:
        form = DFileForm()

    context = {'form': form}
    return render(request, 'asd/uploads.html', context)


@login_required
def details(request, file_id):
    file = DFile.objects.filter(owner=request.user).get(id=file_id)
    context = {'file': file}
    return render(request, 'asd/details.html', context)


@login_required
def delete(request, file_id):
    try:
        file = DFile.objects.get(owner=request.user, id=file_id)
        filename = file.filename
        file.delete()
        messages.info(request, "Файл {0} удалён!".format(filename))
    except DFile.DoesNotExist:
        messages.info(request, "Файл не найден!")
    return HttpResponseRedirect(reverse('asd:index'))


def download(request, link):
    try:
        file = DFile.objects.get(sha_sum=link, link=False)
    except:
        messages.info(request, "Файл не найден!")
        return HttpResponseRedirect(reverse('asd:index'))

    file_path = os.path.join(settings.MEDIA_ROOT, str(file.path))
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + file.filename
            return response
